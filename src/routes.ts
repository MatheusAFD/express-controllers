import { Router } from "express";
import ClientController from "./controllers/ClientController";

const router = Router();

router.get("/clients", ClientController.getClients);
router.get("/clients/:id", ClientController.getClientsById);
router.post("/clients", ClientController.createClient);
router.put("/clients/:id", ClientController.updateClient);

export { router };
