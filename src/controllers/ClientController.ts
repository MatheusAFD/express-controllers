import { Request, Response } from "express";

let clients = [
  { id: "1", nome: "Matheus Araújo", telefone: "85985440099" },
  { id: "2", nome: "João Batista", telefone: "11933410099" },
  { id: "3", nome: "Maria Clara", telefone: "569833440559" },
  { id: "4", nome: "Matheus Silva", telefone: "45985440099" },
];

class ClientControler {
  getClients(request: Request, response: Response) {
    return response.status(200).json(clients);
  }

  getClientsById(request: Request, response: Response) {
    const id = request.params.id;
    const client = clients.find((value) => value.id === id);

    if (client == undefined) {
      return response.status(400).send({ error: "bad request" });
    }
    return response.json(client);
  }

  createClient(request: Request, response: Response) {
    const client = request.body;
    clients.push(client);
    return response.status(201).json(client);
  }

  updateClient(request: Request, response: Response) {
    const id = request.params.id;
    const body = request.body;

    let client = clients.find((value) => value.id == id);

    if (client === undefined) {
      return response.status(400).send();
    }

    client.nome = body.nome;
    return response.status(200).json(client);
  }
}

export default new ClientControler();
